---
title: "the_wolf’s Guide on How to Create Your Own Workout Routine"
output:
  html_document:
    toc: true
---

> the_wolf’s Guide on How to Create Your Own Workout Routine. The post Contains pretty good information and it's a nice read. Altho some of it is also fairly outdated as industry standard is concerned. But it should get you pretty good gains anyways and there is some good stuff to learn in here. Sourced from Scooby's website, which is no longer online. Contains a full body, upper lower split and a body part split. The full body and upper lower ones are pretty good but I would advice against running the body part split since the frequency for legs and back is not optimal. ~ Clemens

# Introduction

OK, despite the fancy name and a content table, this is going to be a pretty clear and straight-forward guide on how to create your own workout routine, or, at least, to check whether it’s right or not. We get this kind of question on the Bulking up board all the time, be it users who have a routine that needs to be rated, or they don’t even know how to start putting a routine up. Having viewed (and answered) lots of those questions, I came to observe the fact that the problems we see in these routines are very similar, or, to say it in a different way, that most of the people make the same mistakes, which are pretty easy to correct, as they aren’t tightly related to a specific trainee. Let me say this once again: just a few people on the forum are advanced enough to make the advice offered in this post obsolete (and I expect those people to already know something on how to create a routine Wink); for the rest (and I’m sorry if this is going to sound rude, but it is true), 99% of advice given here will work. Bodybuilding is extremely complex when we try to grasp everything known about it, but, in its core, it is still very simple and easy to understand and apply. Please take note of that. Squats work the legs, pullups work the back, and although many subtle variations exist, these two facts are, even when as stripped as I’ve written them here, true and easy to implement to reap gains. Of course, if you have an injury or a problem (a real one, not falsely perceived by yourself), some exercises are not for you, and that’s perfectly okay.

Another note is in order here: the forum audience consists of both the people who work out at home gym, those who workout at regular gyms, and those who are just starting at home with no equipment. The last group has, at least in my opinion, its need covered through Scooby’s advice on bodyweight beginner routines, as well as advice provided in the Bulking up section. So, this guide assumes you to have some equipment available. It really doesn’t matter how much, as I’ll try to offer alternatives to some exercises and plans, but, ideally, your (home) gym should include: a dumbbell set and a barbell with enough weight plates, and adjustable bench, preferably with leg extensions/curl attachment, a squat rack (or anything which can allow you to squat), a pullup bar, somewhere to do dips on (Scooby has shown as that all you need is a pair of chairs), and, if possible, a high-pulley cable setup. I know this may seem like a lot, but after you’ve been into fitness for several years (and plan on doing it for a long, long time Wink), you’ll probably acquire/make yourself all of the listed bodybuilding tools.

Now, despite the fact some people have expected this subject to arise later, in the “Splitting it up” section, I’m going to take the time here and discuss what is, in my opinion, the most important decision you have to make before designing a workout, and this is the thing I ask our forum users first when they request a routine. So, the majestic question is: How often do you plan to work out?
Despite seeming simple and not to so important, the answer to this question pulls along everything else we’re going to discuss in this Guide. But first things first, as giving an answer is not as easy one might think. How often you’re going to train is dependant on many factors, most important of which are (in hierarchical order in which they are listed):
1) How long have you been into fitness/bodybuilding (what’s your level of experience)?
2) How much time do your other duties/activities leave you with?
3) What are your specific goals?

Let’s cover all of them:

1) Your level of experience is what should come first. Beginners tend to recuperate faster than people who are longer into working out, but they can also overtrain more easily. Someone who’s been working out longer might want to increase the frequency of workouts, or cut it down. Well, to cut this short, I’ll say my opinion on the matter:

a) If you’re a beginner (and here I’m not applying Scooby’s definition as someone who’s been lifting for less than 6 months – beginner is anyone under 17, and anyone who has never been lifting in his life, or hasn’t been lifting for quite some time, up until the point where they have a need to push themselves more than Scooby’s beginner plan offers) – stick to Scooby’s beginner plan, and bodyweight-train 3 times a week.
b) Intermediate lifters (those who can’t get a proper workout out of bodyweight-only routines) – two options are available. First one is working out 3 times a week (full body routines, but we’ll get there later), and the latter is 4 times a week (upper/lower splits).
c) More advanced trainees know best what’s for them, but I’ll outline some options: 4 or 5 DAW split routines, 3 or 4 DAW full body routine, 3 on 1 off training.

2) What I’ve written above is just an idealization – most of the time, people either have to work, or study, or have other, more important things in life to take care for, and this makes a, for example, 3-on-1-off routine somewhat difficult to practice. I’ll just state my opinion here: never allow bodybuilding to come before your school, college, work, family, etc. As Scooby said it: “Bodybuilding is the means, not the end.” Wink

3) Having specific goals is more likely to occur with more advanced trainees, and this can affect your training frequency. For example, let’s say your arms are lagging. So instead of doing them with other upper body muscles, you decide to give them a separate day, which effectively increases your training frequency by on(c)e (a week). A general rule also follows, and that is that weakpoints dictate the whole routine. Now that some of you rejoiced, thinking I just gave my blessing with working chest and biceps 4 times a week, it’s time to come back down to earth. Most people who give their routines to be inspected do not have weak points, or at least, no weakpoints which would require specific addressing. So, this third point is off for most of the people on the forum. Again, this may sound harsh, but if you have 12″ arms, your bicep peak is the last things you should be worrying about (and this, of course, applies to many other cases). Trust me (and it can be seen on the forum), most people who start off early on specific training (for example, jump straight into the advanced routine) sooner or later realize their mistake and experience the best gains after switching to a routine more appropriate to them.

Now that you have decided how often you’re going to work out, you should probably know that this affects pretty much everything concerning your training. Again, I’ll clarify this by example: If you’re doing a 4 DAW upper/lower split, you simply don’t have time to do 3 different exercises for your triceps, or to do 5 sets of bench presses. Those 2 days of upper body training need to be dense, and require cutting out all the additional exercises and work which would probably be done on a classic, 5 DAW split routine. Similarities, of course, extend to other splits, and I hope that you’ll, by the end of this article, be able to recognize and apply them properly.
OK, with this settled, let’s say something about exercise choice and what to train. Wink

# Exercise choice

This part will be written in a somewhat different manner than expected, so instead of simply listing best exercises for each bodypart, I’ll put in some important workout-related facts which most people don’t seem to follow (and probably understand). So, here we go:

1) You must work out your entire body
I cannot overemphasize this one. This is the first thing one must realize before starting to work out. Everyone starts out with just ABCs (Abs, Biceps, Chest) in mind, and don’t realize soon enough that this approach is wrong for two reasons: first, it results in an unbalanced, injury prone physique that does more harm than good, both from a medical and aesthetical perspective, and second, is hampers your gains – trust me, your chest can’t grow out if your back isn’t there to support it.

2) You should focus your training on several major compound exercises
This one is also very important. Everybody likes curling and doing flys and leg extensions, but also avoid rowing, chins, squatting, etc. The reason for this is simple: compounds are hard, require near perfect form, and add the mental toughness component into the training. People also wrongly think that, exemplum gratia, pullups work the biceps a lot more ineffectively than curls do, and as most beginners tend to think they know what’s best for their bicep, they sit and enjoy themselves curling a dumbbell, without even sweating. The positive effect of compound are hard to grasp in a few sentences, but I’ll try to outline the most important ones:

a) Compounds work multiple muscle groups, which include stabilizer muscles isolation exercises usually don’t target. This means you work a much larger portion of your body doing squats than leg extensions, which, I hope it’s clear, is definitely a good thing. Also, compounds make your body act in a more natural way, so to speak, as they engage the muscle groups which are naturally made to work together, resulting in a more functional and less injury-prone physique. This forum isn’t comprised of pro bodybuilders who are only into adding muscle mass, sacrificing health and general good shape for the sake of it. Wink
b) Compounds allow much higher weights to be used, resulting in more muscle fiber stimulation. This one is somewhat clear, but I’ll explain so no misinterpretations arise. Of course you can move a lot more weight deadlifting than leg curling – after all, it’s your entire body that’s lifting the bar off the floor. But what I’m trying to tell here is that the weight load applied to hamstrings alone is much greater in the DL than it is in the isolating leg curl (but we’ll get to more detailed hamstring training later).
c) During the compound lifts, you burn A LOT more calories than when doing isolation exercises. Just look at the example I’ve show before, which almost everyone can testify it’s true: you’ll sweat and huff and puff a lot more when doing pullups than curling (or, at least, it should be so).
d) Compounds allow your body to produce more hormones, which contribute to larger muscle growth. This topic is still controversial, so I won’t sticky it out or explain it further.

Now, does that mean isolation exercises are to be completely thrown out? My stand here is no, and here’s why: I am a bodybuilder. Not a powerlifter, not a strongman, not an athlete looking for sport-specific performance enhancements. My goal is to build muscle, and in that process, isolation exercises sure do have their rightful place, so I do include them into my workout routines (although they are stapled upon compounds).

3) Avoid machines whenever possible
The issue here is, in its essence, similar to the conflict between compounds and isolation exercises. Using free weights is tougher, but involves much more stabilizer muscles and allows you to follow natural movement patterns. On the other hand, machines offer “safety” and feel more comfortable working with. I won’t go into much detail here as the answer is simple: use free weights whenever possible, except for the exercises which can’t be done without a machine (such as leg extensions – if you have a leg extension machine, don’t bother trying a dumbbell to your ankle Wink). Also, stay away from the dreaded Smith machine – although allowing you to move lots of weight (but just virtually, as it is a counterweight machine, which means you’re moving just half the weight most of the time), and allowing you to do some lifts without a spotter, in the long run it ruins the joints and leads to severe muscle imbalances by completely excluding most small, assisting muscle groups.

4) There’s much more to working chest out than bench pressing
Much has been written on the forum about the bench press (I here mean flat BB bench press), and how it doesn’t do the best job when building the chest. I’m not going to go into those kind of discussions here, but rather add some tips about exercise choices for chest training. I’ll sum this up into a simple statement: to build chest, you need a main press, an incline press, and a fly. Now let’s elaborate each:

a) Main press – this exercise should be a staple of your chest routine, and if you had to do just one, this would be it. This exercise provides the best overall chest development, as well as putting shoulders and triceps to work. The best exercises for this are: pushups, flat presses (be it BB or DB), and chest dips.
b) Incline press – for the sake of more aesthetically pleasing appearance, your chest needs to be “brought up”. Hence, we do incline work, which just consists of doing regular chest work at a low to medium incline bench (15-65°). There’s not much wisdom here – grab a barbell or dumbbells and press on this incline bench.
c) Flys – Scooby swears by them, and most people around here know how great a pump and burning sensation this isolation exercise brings to the chest. You can do both incline and flat flys, and I recommend alternating between those two each workout.

Also, one note is at hand here: when performing chest exercises, try to minimize your shoulder movement by keeping your shoulder blades tight against each other. Otherwise, the force transmission between your arms and chest is not effective and results in shoulders having to take up most of the pounding, which leaves chest worked minimally. Bear this in mind. Wink

5) Please, do work out your back
Okay, unless you’re a professional rower, your back isn’t “already pretty developed”. Period. Most people start their beginner programs with pushups, maybe dips, and similar pushing exercises. Pulls, such as rows and pullups, are overlooked and usually completely forsaken. Also, most people can’t find joy in working the back out. I know this myself: nothing feels better than watching my veins popping out when doing reverse preacher curls. But when I go to rowing, I can’t even see the muscles working! Hopefully, many of you will, eventually, realize how much fun can working out the back be, and how good this bodypart looks when it’s nicely developed. But even until then it’s imperative to work your back out for the sake of your joints and muscle balance. Again, I’ll compress my basic idea into one, short sentence: to build great back, all you need is a vertical pull, a horizontal pull and a lower back exercise. And again, let’s clarify it:

a) Vertical pull – here I exclude vertical rows, which are a upper trap/bicep/medial delt exercise, and focus on one major lift – the chins. I use this as a crown term form vertical pulls done with pronated grip (pullups), supinated (chinups), and neutral grip (these don’t have a specific name). Nothing beats these exercises when it comes to widening you back, putting mass to it and developing your other pulling muscles (biceps, for example) alongside them. A must. Note here than chins should always be placed before pulldowns, according to the “free weight over machines” principle. Also, if you do chins, there’s really no need for pulldowns.
b) Horizontal pull – a.k.a. a row. I believe only one rowing exercise is necessary for great back thickness, but if someone wants to do two of them, be my guest. Right now I can’t think of a rowing exercise which I would tell you to stay away from (except maybe Smith machine bent-over rows). Bent-over BB rows, DB rows, seated rows, T-bar rows, you name it, all do a great job at developing the back.
c) Lower back exercise – although hyperextensions and good morning (real ones, with the bar on your shoulders, not what Scooby shows) are great for isolating the lower back, I won’t recommend either of those to be used as a primary lower back exercise. My view here is pretty clear: deadlifts. Nothing beats the amount of work your body will have to exert when this exercise is used. Lower back soreness is guaranteed, and it’ll surely add to your upper back thickness as well. One may now ask, why not do the deadlift on the leg day? It is, after all, a primarily lower body movement. As you’ll see later in the guide, I always like to separate squats from deadlifts, or, at least, no to do them on the same day, as I find them too taxing and form-dependant to be done side by side. Hence, I always recommend doing DLs on your back day, and squats on the leg day.

Another exercise is worth mentioning here, and those are pullovers. Although there doesn’t seem a general consensus on what muscles does this exercise primarily target, I can tell you it is a very natural movement, which often occurs in the real life, and should be done for at least a set or two. The serratus anterior (intercostals) loves it as well. Wink

6) What to do with shoulders?
People, in my opinion, usually overwork their shoulders by adding lots of unnecessary exercises and excessive number of sets to their routines. The deltoids are small muscles which get their share of work with almost any upper body movement imaginable, so if your delts aren’t growing, it might be because you’re actually overtraining them. Yet again, a comprehension is at hand: to build shoulders, you need an overhead press, a lateral raise and a rear delt exercise.

a) Overhead press – this is the best compound exercise there is for shoulders, and does a great deal for the entire upper body, if done properly. The press I recommend is the standing barbell overhead press, a.k.a. the military press. Not only will this quickly pack mass on your shoulders, but will also add great strength to your core needed to balance the weight over your head. Seated presses are also okay, either done with a barbell or dumbbells. Smith machine is not an option. If you were to do just one exercise for shoulders, presses would be it.
b) Lateral raises – medial delts are the ones who mostly contribute to the “wide shoulders” appearance, and are usually found lacking with many people. The exercise Scooby shows on his site is the L-lateral raise, which, due to the advantageous leverage, allows for more weight to be used. The straight arm lateral raises call for less weight to be used, but still make the best work for medial deltoid development. Low-pulley cables also make great work here.
c) Rear delts – developing the rear delts is a necessity is you are to keep your shoulders healthy and in balance. This part is often found most lacking, compared to the front deltoids, which has much to do with the fact I’ve talked before, and this is overdoing pressing movements at the expense of doing less pulls. Good rear delts will also help your posture. The single best exercise for these are the reverse flys, although rear delt rows and facepulls do the job as well (and reverse peck deck, reverse cable crossovers and some other gym-specific exercises).

Some of you may now ask: “But where are the front deltoid exercises?” In my experience, most people have their front deltoids way out of balance with the rest of the shoulders which is, again, due to all the pressing they do. Every pushup, dip, or bench press you do will contribute to the size of your front delts, not to mention that these are the muscles which are mostly engaged during overhead pressing as well. So, for the sake of balance, I usually don’t put any front deltoid exercises in my programs.

In this section I’ll also include the advice on traps. I’m probably not the best person to give trap workout advice, since I almost never work my upper traps out. But I’ll still give some general guidelines – shrugs and upright rows are the sole exercises you need. Behind the back shrugs tend to target the middle and lower traps more, and make the movement harder to cheat. While performing upright rows, making the grip wider takes away the stress from traps and puts it onto medial deltoids. My advice is to do traps with shoulders for a couple of sets.

7) More is not always better – stop overtraining your arms!
I had to write this with the exclamation mark at the end, otherwise I feel people wouldn’t read it properly (and I’m still sure most people won’t listen to what I have to say in this section). Most of the routines I’ve seen on the forum are arm based – arms are done a couple of times a week, or only once with somewhere between 20 and 30 sets for biceps, and 5-10 for triceps, and most people are under the impression this will make their arms get bigger. The first step to having big arms is doing the compound exercises for the chest, back and shoulders. These alone should be enough for an intermediate bodybuilder, as they’ll provide adequate stimulation. More advanced BBers may rely on isolation exercises more, but the fact still stands: your arms are getting plenty of work during other upper body lifts, so it’s easy to overtrain them by doing excessive sets/exercises on the arm day. Now we’re at it, let’s cover the “arm day” question: “Do the arms need to have their separate day, or should they be done with other major upper body muscle groups?” (This, of course, applies to split routines only.) My advice would be: find out for yourself. Doing arms together with chest or back has its benefits, but some people simply can’t make their arms grow that way and need to have a separate day to give full attention to them. Again, do what you feel is best for you. Another thing to mention is the amount of work to be done. I always recommend doing the same amount of sets for both biceps and triceps (and sometimes even supersetting the exercises).
OK, with this settled, let’s go to the exercises:

a) biceps – I think Scooby’s site has this covered pretty well. The only note I’d take is to do your DB curls with a supination, i.e., start in the neutral (hammer) position, and finish in the supinated one (ideally, you’d start at the pronated position, but that can get hairy, so we’ll skip it). Remember, biceps is also the supinator of the forearm (not only it’s abductor), so both of these aspects need to be taken care of. Another note would be to try to do curls on an incline bench – this really gets you a good stretch, which is also beneficial for muscle growth. And remember: just 2, maybe 3 curls are necessary (if you asked me, those would be DB curls, incline hammer curls and concentration curls) – doing 10 exercises for the biceps won’t make them grow any faster!
b) triceps – Scooby’s site also has some good advice here, but I’ll add some. If you have a cable setup, do tricep pushdowns, be it with a straight bar, a V-attachment or with a rope. Experiment with grips as well, it’s truly a terrific tricep exercise. Doing close-grip bench presses is also great for overloading the triceps in the low rep range (4-8).

Cool What about abs?
I won’t get into detail concerning the ab training, as I could write as long an article as this one concerning the subject. I’d just list some of mine favorite exercise: Swiss-ball crunches, hanging leg raises (bring your ankles to the bar), Russian twists and static hold hanging knee-ins.
9) Your legs constitute a half of your body – don’t forsake them!
This one is probably the most common error beginners make, once they go past doing 200 rep curls three times a day. Most of the time they have similar excuses as well: “I find my upper body more important right now.”, “I am a soccer/basketball/chess player, my legs are developed enough.”, “I do cardio everyday, it’s enough for my legs”, or this one, which really makes me mad, “I go drinking on Friday so I don’t get time to do them.” I hope it’s clear that all of these are just excuses, and pretty worthless and cheap ones. Take note of the title of the section: leg are half your body, and if you don’t train them, you’re missing en enormous boost of overall muscle mass (plus you’re turning yourself into a chicken-legged comic book character). So I hope this is clear: you must work your legs out. I don’t care how painful, exhausting or inconvenient it is – you’ll have to get used to it (I find it interesting that people like to complain about the pain during squats, but never about pain during bench press or curls).In this particular section, we’ll focus on the thighs, i.e., the main compound exercises that build quads, hamstring, glutes and hip muscles up. Similarly to back training, the compound exercises priority principle is accented here as well. Few are good leg exercises which are purely isolation, and their usage is often limited to machines which can only be found in gym. In my opinion, only the leg extensions and curls are worth mentioning, as additional exercises to the main compound lifts. Another note should be made, and this is that the separation between “quad” and “hams” exercises is pretty vague. Squats and lunges work hamstring and glutes besides quads, deadlift is not only a hamstring movement. Hence, to say an exercise is for “quads” or “hamstring” has little meaning for these compound lifts.
People often ask what would be the minimal leg routine, consisting of just two or three exercises. The answer here is simple, as these exercises should be a staple of any leg routine:

IMPORTANT: Before you do squats at home, watch this video.a) Squats – if you still aren’t aware of the importance of squats, just type it into Google and read several articles. This is truly not the place to discuss it. Squats are to be done, period. From other variations, I’d say you should definitely try front squats and hack squats (with or without a machine). Now onto more important topic: how to do squats if you don’t have a way to get the bar up to your shoulders (and you can’t execute a proper power clean). It’s perfectly okay to do squats with dumbbells, although this will, eventually, limit the amount of weight you can move (and with squats, this should be a serious number). If your dumbbells don’t satisfy you, try split squats or single-legged squats for more resistance. Another commonly asked question is does the leg press substitute the squat. The answer is no, for reasons similar to why pulldowns are not pullups. However, leg press is still a great exercise, and can be very well suited for taller trainees (as would front squats be), compared to back squats. As for the question which I’m sure will eventually arise, “How deep to go on my squats?”, I do now have a definite answer. I’m a proponent of using full range of motion on all exercises, which means going past parallel on squats, but not all people share my opinion so I won’t dwelve into further discussions concerning this one. For more information, check out my previous “lengthy post”, called Full vs. Partial squats.
b) Stiff-legged deadlifts – this is what Scooby calls “Good mornings” on his site, a deadlift variations which gives more emphasis one the glutes and hamstrings. Of course, I still do imply you’re doing regular deadlifts somewhere else in the week – if not, as in, for example, a full body routine, that do regular deadlifts instead of the stiff-legged variant. Although some will disagree this to be a compound exercise (the movement occurs only at hips), I still consider it as such because of the several important muscle groups which work together in it. Again, a must for good hamstring development. Try the Romanian deadlift variant as well.
c) Lunges – Unlike squats, lunges are a unilateral movement with a more complex motion involved, which makes it unique and more difficult to perform. The story here is similar to the one connected with squats – although considered a primarily quad-targeting movement, the lunge develops the entire thigh. Walking lunge variant is even more challenging, and should surely be tried (if space allows for it, of course Tongue).As for the other thigh exercises, I’d mention just three of them: Leg extensions are not, I repeat, are not a complete thigh building exercise and do not replace nor squats nor lunges, but can be used as an auxiliary quad exercise. On the other hand, I consider leg curls to be crucial for complete hamstring development in conjunction with SLDLs, as those two enable the hams to develop both of their primary functions (abduction of the tibia and extension of the hips). The third exercise very worth mentioning are the glute-ham raises, which require almost no equipment, but can make your hamstring ache like they never have. It’s definitely worth trying, whether you have the equipment necessary for SLDLs and leg curls or not.
Yet again, before the end – do work out your legs. Enough said.

10) Last but not least – the calves
Similarly to abs, the calves are followed by lots of confronted opinions, which mainly don’t agree on the frequency/volume relations in their training, but we’ll get to his aspect later in the guide. Right now, let’s just focus on calf exercises. For full calf development, both the gastroenciemus and soleus part of the calf have to be trained, and as complex as it sounds when written this way, it’s really a simple thing to conduct: gastroenciemus is trained when the knees are locked (tibia in line with femurs), while the soleus is trained when there’s an angle between those two bones (i.e., the knees are bent). So, two exercises are needed:

a) standing calf raises – these are the ones Scooby describes in his calf video. You can do them unilateral or bilateral, depends on you. Some elevation of the front of the foot is desired for the greater stretch at the bottom position, but you don’t need the accessory Scooby has (although it’s a very simple one to make) – you can just put a plate or two and stand on them. The execution of this exercise is pretty straightforward, but some issues may arise with trainees who work out at home, and these issues concern using weights. The situation is simple at the gym – your approach the calf raise machine, which has lots of weight added on it, put the comfortable pads on your shoulders and just do the exercise. However, we who train at home face a problem how to put the weight on. Calves are VERY strong muscles, and the weight you can use on them progresses rapidly, and soon exceeds the weight you use while squatting or deadlifting (doing 1000 lbs on the calf machine is not such a big deal), so a dumbbell or two method Scooby uses quickly becomes obsolete. In my experience, you have two options: the first is to load the bar up on the squat rack, and then do calf raises with the bar on your shoulders, and while simple, this approach can be very painful for the shoulders, especially if the weight used is much higher than those used at squats, and at higher reps. Trust me, I speak from experience. The alternative method is to have the bar on the floor, deadlift it and hold it in your hands until the end of the set, then put it back down. I trust it’s clear on how this approach can be problematic, as each rep involves a heavy deadlift followed by a not-so-easy forearm static hold. Again, I’ve tried both methods, and ultimately, as the weights became higher and higher, ended up with the first one. Right now I’m trying to come up with a padding of some sort which would relieve the pain of having the bar on my shoulders. of course, there is a way to (slightly) increase the weight you can use – try a weight belt or, if you don’t have one, puts some weight into a backpack (or anything similar).
b) seated calf raises – these ones are also simple – sit down, put your legs in such position that the angle at the knee is slightly greater than 90°, and than do calf raises. Nothing special about this one as well, and the loading is much simpler than people think – just stack lots of plates on your lower thighs (this does require some preparations, but it’s surely conductible).

Again, it’s important to utilize both of these exercises into your routine.
Another exercise I’ll mention are the donkey calf raises. Again, if you’re a gym user, this one won’t pose any problems to you, but at home we face the problem of adding workload. Arnold’s method of having multiple men on your back isn’t too practical nor safe for many of us, but so is holding a barbell in your hands while at the bottom Good Morning position (or, even worse, at your upper back). For these reasons I won’t list donkey calf raises in my routines, but it’s still a great exercise to perform.

# Sets, reps, rest and tempo

With the exercise choice covered as much as possible, it’s time to pay a little more attention to the second important aspect of workout design, and that is creating set/rep schemes, which includes deciding upon adequate rest times and tempo to be used while performing the exercises. The problem I faced several times during the process of writing this guide is becoming clearer and more overwhelming now. As you might have guessed, this problem concerns the amount of simplifications I have to do to make this topic as small as possible. All of those, sets, reps, rest and tempo, have hundreds upon hundreds of pages written about them, and these are all very complex and exhaustive topics which are hard to cover in a simplistic manner I intended to follow in this Guide. So take note of this: I’ll try to present some basic, generally accepted ideas, and the content outlined here doesn’t nearly reflect all the knowledge that we possess on the subject. But again, I feel that this level will be appropriate for the audiences to which the Guide is aimed at. So, let’s get down to business.

1) Sets
As mentioned earlier, I’ll try to keep the story as simple as possible. The “How many sets” question is very important when designing a routine as it basically defines the amount of work you have to do in a single workout. I don’t believe in extremes here, so I will discard both high-volume routines (such as those which Arnold used, or which the pros supposedly use, and this means 20+ sets per bodypart), and the low-volume routines (such as HIT and DC training, which rely on a single working set (which is extended, then turns into a dropset, a partial rep, a forced negative and finally into a static hold – holy muscle trauma!)), and focus on medium-volume, balanced routines which can, by my estimations, be carried out in 45 min tops.

The word “balanced” here is related with two things: the first is balance between the amount of work given to certain bodyparts, i.e., that the chest should always do more than biceps (which people often forget). The second balance is between opposing muscle groups, which means, e.g., that chest and triceps combined will do roughly the same amount of work as back and biceps. There is a third kind of balance here as well, the balance between the amount of work done on certain days, but it can vary a lot more so I won’t get further into explaining it.

I hope it’s clear that there exists a difference between splitting sets up in full body routines, upper/lower split and real split routines. In a split routine, for example, you might have a chest day in which you do 12 sets for chest, while when performing an upper/lower split, you just have two upper body days, and in each one just enough time to do 3, maybe 4 sets of chest exercises (we’ll get to this problem in the “Splitting it up” section). The table outlined below shows the relations between the amount of work given to certain muscle groups as I view (and often apply) it. Let’s repeat it: the numbers in this table show how much sets you should do for a certain muscle group in relation to other muscles.

```
Muscle      Sets
——————————————————
Chest       12
Back        12-14
Shoulders   6-8
Traps       2-4
Triceps     6-8
Biceps      6-8
Forearms    0-2
Abs         4-6
Thighs      12-18
Calves      6-12
```

A couple of notes are in order here. First, the number of sets done for the chest and back is far greater than for any other upper body muscle group because compound exercises used to train them involve these other muscles to a great extent. Second, I don’t believe in the necessity of working the forearms out. I include reverse curls and hammer curls into my routines, but believe that the bulk of forearm development comes from all the heavy compounds that you do, and that forearms, due to this, work almost every day and don’t need additional stimulation. Third, I haven’t separated quads from hams for the reasons I already spoke about, and that is the fact that it’s often hard (and sometimes impossible) to separate whether a particular exercise works just one or the other. However, the biggest explanation concerns calves. As I’ve mentioned earlier, people have very different views on how to train your calves. Some believe in high-volume, while the others (myself included), who are blessed with good calf genetics, rely on a smaller amount of work to stimulate them. Which approach works for you is, of course, something you’ll have to find out by yourself.
Now onto something more practical. A rule of the thumb I’ll be using, and which I advise you to do as well is: do around 3 working sets per exercise, and pick the number of exercises accordingly to the table shown above (in relative terms, of course). This is a very crude approach, but it works. Also, do note the “working set” part – I always recommend a major compound exercise, or any exercise done first in the workout (which will, most of the time, be a major compound), to be preceded by an adequate amount of warmup sets (1-4).

2) Reps
The general rule of this section will be expressed negatively: if doing a weighted exercise, do not, except in some very special cases, do less than 4 reps, and more than 20 reps. Anything between those two values works (things work outside those boundaries, but should be left for special training, such as powerlifting and endurance training). I’ve noticed that people usually don’t realize this, and consider that doing 100 reps is better than doing 10 reps, and it builds more muscle. This is an utter misconception which simply doesn’t work, and I hope everyone realized this very early into their training. If doing more reps meant doing better work, nobody would need to invest into weight plates – we’d just take a bottle of water and do a couple of hundred reps. Again, this view is false.

The amount of reps to do is closely related to the weight used. My view is that you should always aim to use the maximum weight you can for the given amount of reps. For example, if you know you can do 12 reps with 50 kg, why bother using 40 kg for that same number of reps, as it will surely provide less stimulation to the muscle. So, in my opinion at least, always aim do to your best, and to, hence, make your progress more rapid and evident. Deloading phases, in which you deliberately use less than maximum weights, are necessary and have their purposes, but normal training calls for a more efficient approach, which comes from giving your best all the time.

You should also note another thing: the amount of reps done (or, if we are to look from the other corner, the percentage of 1 RM used) has some other effects as well. For example, using more weight requires for a different type of “triggering” of the central nervous system, and is more tiring to it, while doing 20 reps calls for quite a mental endurance and strength needed to push through all the reps with perfect focus. To make it a bit simpler, let’s look at the following rep range division:

a) 4-7 reps – low to moderate rep range, means using weights from 80-90% of your estimated 1 RM. As all heavy lifting, it’s taxing on the joints and CNS. Greatly develops strength alongside mass, but usually suffers from a lack of pump, i.e., the low number of reps often fails to usher lots of blood into the muscle and provide burning sensation as well.
b) 8-12 reps – moderate rep range, 70-80% of 1 RM. This is called the “ideal hypertrophy” rep range, as it, supposedly, provides maximum muscle growth.
c) 13-20 reps – moderate to high rep range, 50-70% 1 RM. Although still being good for hypertrophy (especially when in need for a “lighter” training), this rep range leans towards muscle endurance rather than strength development. Offers great pump, as is suitable for warming up and cooling down sets.

Most of the time, I tend to use moderate rep range for hypertrophy, but I also like doing low rep range for heavy compounds (deadlifts especially), and high rep range for some finalizing isolation exercise.

3) Rest
The question we’re asking ourselves here is: “How long should I rest between sets?” Although this does depend on one’s training level (being accommodated to using weights, cardiovascular endurance, CNS recovery capabilities), a general rule does exist, and it says that the amount of time you need to rest is directly proportional to the intensity of the set. By intensity, I don’t directly mean the percentage of the 1 RM used, but by how taxing a set was to your body (and brain Wink). For example, you should rest more after a 10 rep set of squats than you should after a 10 rep set of DB curls. Also, you should rest more after a superheavy set of squats than after a moderate one (unless you’ve been doing 20 rep death squats, you rest for days after that one Tongue). I think you’re getting the picture now, but we still have to determine some real numbers to use.

I am, personally, a fan of shorter rest between sets, and I believe this to be a better approach for several reasons:
a) Shorter rest improve your muscle endurance, and your cardiovascular endurance as well (try doing several high-rep sets of squats with short rest in between – rest assured, you won’t need cardio after that).
b) The pump is usually better, as well as the focus on the exercise (thoughts tend to slip away when resting for a few minutes between sets).
c) The entire workout is much shorter in duration.
For a normal, non-superheavy and not-too-taxing set, I rest 1 min. For a something a bit more, between 70 and 90 sec. I only rest for 2 full minutes after superheavy deadlifts and squats (volume squat routine also requires for such a longer rest for time to time). On the other hand, easier exercises, such as leg extensions and curls, require from 30 to 45 seconds of rest. I am also a great fan of supersets, trisets, giant sets, etc., and employ them on a regular basis (for example, my entire calf routine is done with no rest between sets).

The last thing I’ll mention here is a mistake I often witness with people when it comes to unilateral training. For example, someone does a set of concentration curls with the left arm, switches to the right arm, and then starts his rest timer, which is set for, let’s say, 1 min. And for how long do his arms rest? Longer than 1 min, that’s for sure (most likely 80-100 sec). I hope you see my point. When performing unilateral exercises, start counting your rest time after you’re done with the first limb. This is the accurate method for measuring rest time in such cases.
One last note: as you get more experienced with training, the rest you have to do will probably decrease (if you, of course, allow it to Wink).

4) Tempo
The topics of tempo is probably the only one in the Guide who’s title left you confused and not knowing what to expect. This is a very common reaction when this kind of discussion arises for the simple reason of tempo being one of the most neglected aspect of training which, by its importance, doesn’t fall behind sets, reps and rest at all. As I assume most of the forum audience to be unfamiliar with the concept of tempo, I’ll keep the story as short and simple as possible, and limit my ideas on several easy-to-understand advices.

So, what actually is tempo? Simply speaking, tempo is the speed at which you perform your reps, and is defined by four numbers (and is usually written in such a manner): the duration of the negative part of the lift, pause at the bottom, the duration of the positive part of the lift, and (sometimes) the pause at the top. For example, when someone writes 41X3, for squats, e.g., it means you should descend for 4 seconds, remain down for 1 second, then come up as fast as possible and rest 3 seconds at the top.

The next question which probably arises is: “Why do I need any of it? 2 s up, 2 s down works fine for me.” Tempo is just another training parameters, and this means another way of adjusting your workouts. Making changes in performance tempo can be used for busting plateaus, increased muscle growth and targeting muscles in a different way. So I strongly encourage you to give it a try and drop your usual training cadences for new, more variant tempos. Here are some notes which may help you:

a) slowing down the negative can make the positive stronger (the idea behind forced negatives)
b) making a pause at the bottom of the lift makes the so-called “stretch reflex” to disappear, making it harder to lift the weight again (of course, this should only last for a second or two, you shouldn’t rest in that period)
c) making a faster concentric can have positive effect on developing explosive strength
d) holding at the top of the lift (“squeezing” and static holds) also have their benefits
e) always control the descent of the lift (the negative part)
f) changing tempo greatly affects the time under tension – take note of that.

# Splitting it up

So, here we are, at the very summit of our journey. I hope you learned something in the previous sections, and that you’ll be able to use it adequately in constructing your own routines.
This last section will cover 4 types of routine splits, and offer a sample routine alongside it (I just hope most of you won’t simply copy these routines without applying the previously gathered knowledge Wink). Just a note: I don’t include ab work into my routines as I see it to be too trainee-specific, and consider it should be done on off (non-training or cardio) days. Here we go:

### Full body routine
I’ll note this in the very beginning: for a more exhaustive description of full body routines, and the principles which apply in them, read this excellent article by dodothebird, named Full Body Workouts for the Advanced. The full body routine I’ll describe here is suited for novices to weightlifting (after they’ve passed the “beginner phase”), and intermediate trainees who want to try something new. More Advanced trainees (and everyone else interested in the subject) will find out everything they need in dodo’s article.
The full body routine I offer here is a 3 DAW non-consecutive split, done either Mon-Wed-Fri or Tue-Thu-Sat (or Wed-Fri-Sun, but I don’t think this to be convenient for anyone). We have the A and B workout, which differ slightly among each other, and you should alternate between those two: A-B-A-B-A… Each workout consists of 6 exercises, 4 of which are major compound exercises, and two are auxiliary, isolation ones. So, here’s the plan:


```
THE_WOLF’S 3 DAY FULL-BODY ROUTINE

WORKOUT A
squat – 2-4 warmup sets, then 3-4×5-8
incline BB bench press – 3×8-12
(weighted) pullups – 3×8-12
military press – 3×8-12
cable pushdowns (or french press) – 2×8-12
standing/seated calf raises – 3-5×8-12

WORKOUT B
deadlift – 2-4 warmup sets, then 3-4×5-8
bent-over BB rows – 3×8-12
(weighted) dips – 3×8-12
upright rows – 3×8-12
DB curls – 2×8-12
reverse fly (or a rotator cuff stabilizing exercise, such as the Cuban press) – 3-5×8-12
```

### Upper-lower split
These kinds of splits are very effective for intermediate and advanced trainees alike, as they allow you to stimulate each muscle groups twice a week, and out much more emphasis on the lower body than other routines (which is, as I hope you’ve understood, a good thing). The routine outlined below is a 4 day routine, in which you alternate upper and lower workouts. Upper workouts differ among each other by the exercise choice and emphasis on the pushing and pulling motions, while the lower body days are separated between quad- and hamstring-dominant ones. Let’s have a look at it:

```
THE_WOLF’S 4 DAY UPPER-LOWER ROUTINE

MON – Upper body A
flat BB bench press – 1-2 warmup sets, then 3×5-8
(weighted) pullup – 3×8-12
millitary press – 3×5-8
weighted dip – 3×5-8
BB curl – 3×8-12
reverse fly – 3×10

TUE – Lower body hip dominant
deadlift – warmup, then 5×5
leg curls – 3×5-8
split squats (or leg press) – 4×8-12
standing calf raises – 5×8-12

WED – cardio & abs

THU – Upper body B
bent-over BB row – 1-2 warmup sets, then 3×5-8
incline db bench press – 3×8-12
chinups – 3×8-12
lateral raise – 3×8-12
french press (or cable pushdowns) – 3×8-12
hammer curl – 3×8-12

FRI – Lower body knee dominant
squat – warmup, then 5×5
leg extension – 3×8-12
walking lunge – 4×8-12
seated calf raises – 5×15-20

SAT – cardio & abs

SUN – off
```

### Split routine
These kind of routines are by far the most favored ones among trainees, and everyone seems eager to hop into this training regime which has a chest day, a back day, an arms day, and a leg day (or two! Tongue), of course. I tried lots of slit routines, from those split over 6 days a week, via my last routine, done 5 DAW, to my current routine, done 4 days a week. I consider 4 DAW approach to be the best, as it gives you much more time to recover, and working out for not more than 2 consecutive days gives your joints and CNS a bigger opportunity to fully recover as well. I haven’t tried the routine described below (my training system is a bit more complex to be explained here), but if I were to fully simplify my training, this would be THE routine (with slight modifications, of course, there would be less straight sets than here, and more weakness-specific work):

```
THE_WOLF’S 4 DAY SPLIT ROUTINE

MON – Chest/shoulders
warmup with 2 sets of pushups
flat BB bench press – 3×5-8
incline DB bench press – 3×8-12
weighted dips – 2×5-8
(incline) DB flys – 2×8-12
military press – 3×5-8
lateral raises – 3×8-12

TUE – Back
deadlifts – warm up with 2-4 sets, then 3×5
bent-over BB rows – 4×5-8
weighted pullups – 4×8-12
reverse flys – 3×8-12

WED – Cardio & abs

THU – Legs
(front) squats – warmup with 2-4 sets, then 3×5-20
walking lunges – 3×8-12
leg extensions – 3×8-12
unilateral leg curls – 3×5-8
SLDL – 3×8-12
standing calf raises – 4×8-12
seated calf raises – 2×12-25

FRI – Arms
weighted dips – 3×8-12
close-grip bench press – 3×5-8
cable pushdowns (or french press or overhead tricep extensions) – 2×8-12
DB curls – 3×8-12
incline hammer curls – 3×8-12
concentration curls – 2×8-12

WEEKEND – Cardio & abs
```

### 3-on-1-off routine
A 3 day on 1 day off routines are, in my opinion, adequate only for advanced trainees, because they combine the specific targeting of a split routine with the frequency of a upper-lower one. Also, when it comes to 3-on-1-off, I like to mix in the push/pull/legs division of the exercises. Here, I offer two alternative types of push, pull and leg workouts, which are to be carried in this order: push A – legs A – pull A – off – push B – legs B – pull B – off – pull A -… I’ll leave it to you to figure out the best set/rep schemes Wink.

```
THE_WOLF’S 3-ON-1-OFF ROUTINE

PUSH A
flat BB bench press
low-incline DB bench press
incline DB fly
military press
lateral raises
rope pushdowns
DB overhead extensions

PUSH B
mid-incline BB bench press
flat DB flys
weighted chest dips
DB seated press
L-lateral raises
french press
DB kickbacks

PULL A
weighted pullups
bent-over BB rows
upright rows
pullovers
DB curls
hammer curls
reverse flys

PULL B
T-bar rows
chinups
high-pulley rows
rear-delt rows
BB shrugs behind the back
BB curls
reverse curls

LEGS A
squats
lunges
leg extensions
leg curls
SLDL
standing calf raises
seated calf raises

LEGS B
deadlifts
front-squats
split squats
leg curls
leg extensions
one-legged standing calf raises
donkey calf raises
```

### BONUS: A bodyweight-only routine!
This routine is for trainees who have limited or no equipment, or just want to perform some training after their cardio (you run, and then stop in a park or a children’s playground). Again, this is something I’d follow if I weren’t into bodybuilding so much, but just trying to keep myself in shape.
This routine would be done 3-4 days a week, depending on the soreness, and it wouldn’t last more than 15-20 mins. The exercises would be done in a circuit, which would be repeated 3-5 times. If you prefer floor ab work, you can do it at home (don’t forget to throw in some hyperextensions there).

THE_WOLF’S BODYWEIGHT-ONLY ROUTINE

```
a pullup variant – chinups, pullups, mixing grips, adding static holds at the end
dips
bodyweight rows or inverted rows
a pushup variant – Scooby shows a whole lot of them Wink
single-legged squats or split-squats
glute-ham raises
a bar ab exercise – hanging leg raises, hanging knee ins, lever twists, wipers…
```
