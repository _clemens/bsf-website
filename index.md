<center>
<img src="https://cdn.discordapp.com/attachments/890946379030077450/914990283354177547/sketch-1638220975112.png" width="400"/>
</center>

# About BSF Fitness

The discord server is aimed for people who are dedicated to making a positive change to their body. Here we will offer you health, nutrition and training advice to reach your goal with passionate server helpers, and highly educated individuals in relevant subject areas.

Our aim as a server is to get rid of any misinformation that circulates within the fitness community, and provide a friendly and non-toxic for any individual that wants to make a change.

* Discord invite: [BSF on discord](https://discord.com/invite/atpCszx)
* Rate us on disboard: [BSF on Disboard](https://disboard.org/server/688710650507165697)

# Content

* [BSF beginner flagship routine](https://clemens0.gitlab.io/bsf-website/flagship_beginner.html)
* [Our guide to creating your own training split](https://clemens0.gitlab.io/bsf-website/clemens_split_guide.html)
* [All about creating sets and reps](https://clemens0.gitlab.io/bsf-website/sets_and_reps.html)
* [Our guide to basic nutrition for bodybuilding](https://clemens0.gitlab.io/bsf-website/leight_nutrition_guide.html)
* [Source code of the website](https://gitlab.com/clemens0/bsf-website)
