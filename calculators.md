---
title: "BSF Fitness calculators"
output:
  html_document:
    toc: true
---

## BMI Calculator

<div class="container">
<script src="./scripts/bmi.js"></script>
<p>Height (in cm)</p>
<input type="text" id="height">
<p>Weight (in kg)</p>
<input type="text" id="weight">
<button id="btn">Calculate</button>
<div id="result"></div>
