---
output:
  html_document:
    toc: false
---

<center>
<img src="https://cdn.discordapp.com/attachments/748940478250221578/953690078281105408/bsf.png" width="400"/>
</center>

# Which training program should you follow?

SMART goals are are goals that help you setup and evaluate your training progress to see if you actually get out of it what you want. The letters of the word descrive how you should set them up. They ensure that you get to your goals within a specific time and may help to reevaluate them when you don't reach the goals.

* **S - Specific** What do you want to achaive with your training? Mainly bigger biceps? Overal development? Getting stronger? Losing weight? "Looking like X anime character?". Being as specific as you can helps you to narrow down what you really want from training. They may also help you stay true to yourself. If someone for example tells you to do a program without ab work and you really want abs, it may not be the right program for you. Another specific part of training is how often and how long you would prefer to train, this has to work according to your goals and schedule.

* **M - Measureable** A good way to measure your progress is usually to look at the strength on your lifts, a scale for your bodyweight etc. A mirror mirror doesn't give a lot of feedback short term. You you also use a bodyfat calliper or a tape measure for the size of your biceps and other bodyparts. Find a way to measure your program according to your goal. Most of this guide assumes you just want to get overal bigger muscles and get fairly good strength gains as a result or visa versa.

* **A - Achievable** Your goals need to be in the range of what is possible. https://symmetricstrength.com/ is a good website for achievable natural strength standaards. You may want to do research on what you can achieve according to your goals before starting with them. Ask your peers or people in the server if you don't know where to start. Worst case scenaro you may end up with body dismorphia or an eating discorder because your specific goal was not (naturally) achievable. You may want to look up to natural bodybuilders from either the present in Youtube fitness, or from bodybuilders from the silver age when steroids didn't exist yet. Bellow you can see some images of natural achievable phisiques from current day and from the silver age. Keep in mind that these phisiques take years to create, and that people in these pictures have been training for 10+ years. Most natural lifters are gonna have a very simulair size tho.

<center>
<img src="https://i.pinimg.com/originals/a7/75/88/a775881264babc6719e33bb5dfd0dca5.jpg" height="150"/>
<img src="https://cdn.discordapp.com/attachments/748940478250221578/948940567289671710/Capture.PNG" height="150"/>
<img src="https://4.bp.blogspot.com/-q45fdYMRsnw/ULTPOWnSyII/AAAAAAAAa_I/sa4KlJvzVnM/s640/hcbod.jpg" height="150"/>
<img src="https://keepfitkingdom.com/wp-content/uploads/2017/07/Eugen-Sandow-Keep-Fit-Kingdom-770x472-2.jpg" height="150"/>
</center>


* **R - Relevant** Why do you want to get biger biceps? Think to yourself why you want to achieve this goal. If you simply want to get a better body to actract woman for example you may want to learn to talk to woman instead of deadlifting 200kg. This point is again very important because you get to reflect if lifting for you is just a cope, and may again result in body dismorphia.

* **T - Time-bound** Again https://symmetricstrength.com/ is a good website for some of these strength standaards within a specific time frame. The chart below also shows a very simple depiction of how fast you can gain strength. If you don't get to a specific strength standaard withing a specific time you know you might be doing something wrong and you need to reevaluate your training. The same idea holds true with for example losing weight. If you don't lose x amount of weight per month you may want to reevaluate your diet plan. The table bellow shows how much muscle you can realistically gain as a natural powerlifter / bodybuilder on a REAL program. average lifter doesn’t give it his all but if you sleep right eat a lot and have lifting as one of your top priorities it’s 100% doable in my oppinion. Do keep in mind that everyone is different, and more importantly, has different starting points. If you've never done sports in your life you will gain less muscle than a professional sprinter who starts bodybuilding. (ps. thanks teensyboye for giving the strength standaards).

| **Years of PROPER training** | **Lean mass gained per year** |
|--------------------------|-----------------------------------|
| 1                        | 25 lb, 12 kg                |
| 2                        | 12 lbs, 6 kg                |
| 3                        | 6 lbs,  3 kg                |
| 4+                       | 3 lbs, 1.5 kg               |


| Lift 1RM | Beginner (6 months) | Intermediate (18 months) | Advanced (24 months) |
|----------|-----------------|-----------------|-----------------|
| Squat    | 315 lbs, 140 kg | 385 lbs, 170 kg | 405 lbs, 180 kg |
| Bench    | 225 lbs, 100 kg | 275 lbs, 120 kg | 300 lbs, 135 kg |
| Deadlift | 405 lbs, 180 kg | 455 lbs, 200 kg | 500 lbs, 225 kg |


## Should I change my program to something better?

If your lifting numbers go up with your current training program you don't need to change a thing about it. You shouldn't be scared if it's a "perfect workout routine". You should only change your training program when  you stagnate according to your SMART goals, or for most of you, if your lifts don't go up anymore.

One other reason you may want to change your program is because you simply don't seem to enjoy your current training program. That's perfectly fine you can try another one. Do keep in mind that if you program hop too often you might not see any results. Especially as a beginner you need to learn to be patient and consistant. As a beginner you should do your program for about 4-6 months at least unless you have major issues with it.


# BSF programs

* [BSF Beginner full body](https://clemens0.gitlab.io/bsf-website/flagship_beginner_full_body.html) This beginner program has a foccus on bench, deadlift, squat and arms. The web page contains 3-4 workout plans and templates. It is the program I recommend you run, since I see it as most optimal for strength getting a good looking body.

* [BSF Biginner PPL](https://clemens0.gitlab.io/bsf-website/flagship_beginner_ppl.html) I am personally not a huge fan of PPL programs. Nevertheless lots of beginners do choose to train like this. It is a good program and will get you good gains.

* [Create something from scratch](https://clemens0.gitlab.io/bsf-website/clemens_split_guide.html) This webpage contains information on how to create a training split from nothing. It is not recommended to create a routine as a beginner but if you can't help yourself or have a more specific goal that the other programs listed here don't talk about feel free to check it out. 

# Other programs

* [Greyskull LP](https://greyskull.app/) Great program for beginners. The program is mainly foccus on OHP and Bench press. It it recommended to get the book. or if you just want to get started look at the program [here](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fi.imgur.com%2FlM5YmWG.png&f=1&nofb=1). They also have a nice app.

* [Stronglifts 5x5](https://stronglifts.com/5x5/)
Its not the perfect beginner program out there, but its the most popular and has the most support. Be sure to do the extra arm work or else you'll look like a spider. The program mainly focusses you on getting better at squats. They also have a nice app to get you started.

* **Powerlifting to win** Powerliftingtowin is a great website and youtube channel that talks about Powerlifting as a sport. He was some very good insights on the sport and his program review series is very well done. This review series might allow you to pick one you think is more appropriate for you. The [program reviews](https://www.powerliftingtowin.com/powerlifting-programs/) are available on his website and on [youtube])(https://www.powerliftingtowin.com/powerlifting-programs/)

* [Candito programs](http://www.canditotraininghq.com/free-programs/) Good programs. Extremely populair for powerlifting.

* [r/naturalbodybuilding's Quarantine Workout template](https://www.reddit.com/r/naturalbodybuilding/comments/fivvhv/the_quarantine_workout_template/) The post contains a full PDF with programs and movements you can do with minimal equipment. Probably one of the better bodyweight routines out there for free. There is a screenshot of the novice program so you can start the moment you open the thread if you don't want to read the whole post. But it is recommended to read the post/pdf first. If you want optimal gains don't run this program if you have a gym you can go too.
